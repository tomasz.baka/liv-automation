import bilby
from scipy.constants import physical_constants
import numpy as np
from bilby.core.utils.constants import speed_of_light as c, parsec as pc, solar_mass, gravitational_constant as G


h = physical_constants["Planck constant in eV/Hz"][0] * 1e12  # h in peV*s


class SignPrior(bilby.prior.Prior):
    """
    Prior for sign of LIV parameter. It can be either +1. or -1. Based on Justin's Morse phase prior
    """

    def __init__(self, name=None, latex_label=None):
        """
        Initialization function.
        Uniform probability for -1, +1

        ARGS:
        -----
        - name: the name of the parameters (typically 'n_phase')
        - latex_label: latex label used for the event (ex: '$n_{1}$')
        """
        bilby.prior.Prior.__init__(self, name, latex_label)
        self.minimum = -1.
        self.maximum = +1.

    def rescale(self, val):
        """
        Maps the continuous 0 to 1 interval to the -1, +1
        discrete distribution
        """
        resc = self.minimum + np.floor(val + 0.5) * (self.maximum - self.minimum)
        return resc

    def prob(self, val):
        """
        Compute the probability of having a given value in the interval.
        This is 1/2 for each value here
        """
        return ((val == self.minimum) | (val == self.maximum)) / float(self.maximum - self.minimum)

    def cdf(self, val):
        """
        Representation of the cumulative density function which can
        be used in order to make the samples.
        """
        cdf = (val >= self.minimum) / 2. + (val >= self.maximum) / 2.
        return cdf


def liv_correction_group(
        frequency_array, luminosity_distance, alpha, A_eff, **kwargs):
    """
    calculates LIV correction using group velocity following https://arxiv.org/pdf/2203.13252.pdf

    Parameters:


    frequency_array: frequencies on which to evaluate the correction, in Hz
    luminosity_distance: float
        luminosity distance in Mpc
    alpha: float
        exponent of lorentz violating momentum term
    A_eff: float
        A_eff in units of peV^(2-alpha)

    Output

    correction: np.array
        LIV phase correction factor, so that the overall modification is exp(1j*correction)"""

    distance = luminosity_distance * pc * 1e6
    zeta = -(np.pi * distance * A_eff * h**(alpha - 2.) / c)
    return zeta * frequency_array**(alpha - 1.)


def liv_correction_phase(
        frequency_array, mass_1, mass_2, luminosity_distance, alpha, A_eff, **kwargs):
    """
    calculates LIV correction using phase velocity following https://arxiv.org/pdf/1110.2720.pdf,
    but parametrized in A_eff

    Parameters:


    frequency_array: frequencies on which to evaluate the correction, in Hz
    mass_1, mass_2: masses of the binary, in solar mass
    luminosity_distance: float
        luminosity distance in Mpc
    alpha: float
        exponent of lorentz violating momentum term
    A_eff: float
        A_eff in units of peV^(2-alpha)

    Output

    correction: np.array
        LIV phase correction factor, so that the overall modification is exp(1j*correction)"""

    if(alpha == 1.):
        distance = luminosity_distance * pc * 1e6
        zeta = np.pi * distance * A_eff / h / c
        mchirp = bilby.gw.conversion.component_masses_to_chirp_mass(mass_1, mass_2) * solar_mass
        freq_factor = np.pi * mchirp * G / c**3
        return zeta * np.log(freq_factor * frequency_array)
    else:
        distance = luminosity_distance * pc * 1e6
        zeta = (np.pi * distance * A_eff / (alpha - 1.) * h**(alpha - 2.) / c)
        return zeta * frequency_array**(alpha - 1.)


def liv_binary_black_hole(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, alpha, A_eff, **kwargs):
    """
    Binary black hole model with group velocity LIV correction

    alpha: float
        exponent of lorentz violating momentum term
    A_eff: float
        A_eff in units of peV^(2-alpha)
    """

    gr_waveform = bilby.gw.source.lal_binary_black_hole(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2, luminosity_distance=luminosity_distance,
        a_1=a_1, tilt_1=tilt_1, phi_12=phi_12, a_2=a_2, tilt_2=tilt_2, phi_jl=phi_jl,
        theta_jn=theta_jn, phase=phase, **kwargs)

    correction = np.zeros_like(frequency_array, dtype=complex)
    if correction[0] == 0:  # avoid raising 0 to negative power
        correction[1:] = np.exp(liv_correction_group(frequency_array[1:], luminosity_distance,
                                                     alpha, A_eff, **kwargs) * 1j)
    else:
        correction = np.exp(liv_correction_group(frequency_array, luminosity_distance,
                                                 alpha, A_eff, **kwargs) * 1j)

    if isinstance(gr_waveform, type(None)):
        return None
    else:
        return dict(plus=gr_waveform['plus'] * correction, cross=gr_waveform['cross'] * correction)


def liv_binary_neutron_star(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, lambda_1, lambda_2, alpha, A_eff, **kwargs):
    """
    Binary neutron star model with group velocity LIV correction

    alpha: float
        exponent of lorentz violating momentum term
    A_eff: float
        A_eff in units of peV^(2-alpha)
    """

    gr_waveform = bilby.gw.source.lal_binary_neutron_star(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2, luminosity_distance=luminosity_distance,
        a_1=a_1, tilt_1=tilt_1, phi_12=phi_12, a_2=a_2, tilt_2=tilt_2, phi_jl=phi_jl,
        theta_jn=theta_jn, phase=phase, lambda_1=lambda_1, lambda_2=lambda_2, **kwargs)

    correction = np.zeros_like(frequency_array, dtype=complex)
    if correction[0] == 0:  # avoid raising 0 to negative power
        correction[1:] = np.exp(liv_correction_group(frequency_array[1:], luminosity_distance,
                                                     alpha, A_eff, **kwargs) * 1j)
    else:
        correction = np.exp(liv_correction_group(frequency_array, luminosity_distance,
                                                 alpha, A_eff, **kwargs) * 1j)

    if isinstance(gr_waveform, type(None)):
        return None
    else:
        return dict(plus=gr_waveform['plus'] * correction, cross=gr_waveform['cross'] * correction)


def liv_binary_black_hole_phase(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, alpha, A_eff, **kwargs):
    """
    Binary black hole model with phase velocity LIV correction

    alpha: float
        exponent of lorentz violating momentum term
    A_eff: float
        A_eff in units of peV^(2-alpha)
    """

    gr_waveform = bilby.gw.source.lal_binary_black_hole(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2, luminosity_distance=luminosity_distance,
        a_1=a_1, tilt_1=tilt_1, phi_12=phi_12, a_2=a_2, tilt_2=tilt_2, phi_jl=phi_jl,
        theta_jn=theta_jn, phase=phase, **kwargs)

    correction = np.zeros_like(frequency_array, dtype=complex)
    if correction[0] == 0:  # avoid raising 0 to negative power
        correction[1:] = np.exp(liv_correction_phase(frequency_array[1:], mass_1, mass_2,
                                                     luminosity_distance, alpha, A_eff, **kwargs) * 1j)
    else:
        correction = np.exp(liv_correction_phase(frequency_array, mass_1, mass_2,
                                                 luminosity_distance, alpha, A_eff, **kwargs) * 1j)

    return dict(plus=gr_waveform['plus'] * correction, cross=gr_waveform['cross'] * correction)


def liv_binary_neutron_star_phase(
        frequency_array, mass_1, mass_2, luminosity_distance, a_1, tilt_1,
        phi_12, a_2, tilt_2, phi_jl, theta_jn, phase, lambda_1, lambda_2, alpha, A_eff, **kwargs):
    """
    Binary neutron star model with phase velocity LIV correction

    alpha: float
        exponent of lorentz violating momentum term
    A_eff: float
        A_eff in units of peV^(2-alpha)
    """

    gr_waveform = bilby.gw.source.lal_binary_neutron_star(
        frequency_array=frequency_array, mass_1=mass_1, mass_2=mass_2, luminosity_distance=luminosity_distance,
        a_1=a_1, tilt_1=tilt_1, phi_12=phi_12, a_2=a_2, tilt_2=tilt_2, phi_jl=phi_jl,
        theta_jn=theta_jn, phase=phase, lambda_1=lambda_1, lambda_2=lambda_2, **kwargs)

    correction = np.zeros_like(frequency_array, dtype=complex)
    if correction[0] == 0:  # avoid raising 0 to negative power
        correction[1:] = np.exp(liv_correction_phase(frequency_array[1:], mass_1, mass_2,
                                                     luminosity_distance, alpha, A_eff, **kwargs) * 1j)
    else:
        correction = np.exp(liv_correction_phase(frequency_array, mass_1, mass_2,
                                                 luminosity_distance, alpha, A_eff, **kwargs) * 1j)

    return dict(plus=gr_waveform['plus'] * correction, cross=gr_waveform['cross'] * correction)

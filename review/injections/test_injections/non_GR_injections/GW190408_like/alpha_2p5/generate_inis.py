import os
import glob
import argparse


def modify_ini(file_name, output_ini, outdir, label, prior_name, injection_name, local_gen=False):
    with open(file_name, 'r') as file:
        # read a list of lines into data
        data = file.readlines()
    #set outdir
    idx=[n for n, x in enumerate(data) if 'outdir' in x]
    data[idx[0]]='outdir='+outdir+'\n'
    #set label
    idx=[n for n, x in enumerate(data) if 'label' in x]
    data[idx[0]]='label='+label+'\n'
    
    #local generation
    idx=[n for n, x in enumerate(data) if 'local-generation' in x]
    try:
        data[idx[0]]='local-generation={}\n'.format(str(local_gen))
    except:
        data.append('local-generation={}\n'.format(str(local_gen)))


    #set prior location
    idx=[n for n, x in enumerate(data) if 'prior-file' in x]
    data[idx[0]]='prior-file='+prior_name+'\n'
    
    #set injection file location
    idx=[n for n, x in enumerate(data) if 'injection-file' in x]
    data[idx[0]]='injection-file='+injection_name+'\n'
    
    

    with open(output_ini, 'w') as f:
        for line in data:
            f.write(line)
            
def modify_prior(prior_name, modified_name, alpha, A_eff_min, A_eff_max, log_uniform=False):
    with open(prior_name, 'r') as file:
        # read a list of lines into data
        data = file.readlines()
        
    data.append('alpha = {:.1f}\n'.format(alpha))
    if log_uniform:
        data.append('A_eff = SymmetricLogUniform(name=\'A_eff\', minimum={}, maximum={}, unit=\'$peV^{{2-\\\\alpha}}$\', latex_label=\'$A_{{eff}}$\')'.format(A_eff_min, A_eff_max))
    else:
        data.append('A_eff = Uniform(name=\'A_eff\', minimum={}, maximum={}, unit=\'$peV^{{2-\\\\alpha}}$\', latex_label=\'$A_{{eff}}$\')'.format(A_eff_min, A_eff_max))
    with open(modified_name, 'w') as f:
        for line in data:
            f.write(line)
            
def get_ini_prior():
    path = './'
    
    config_file = glob.glob(path+'*.ini', recursive=False)[0]
    prior_file = glob.glob(path+'*.prior', recursive=False)[0]
    return config_file, prior_file

            

        

def make_directory_structure(label = 'inj', A_eff_min=1e-24, A_eff_max=1e-12, local_gen =False, log_uniform=True):
    cwd=os.getcwd()
    path = cwd
    alphas=[2.5]
    ini, prior=get_ini_prior()
    injection_path=cwd+'/injections.json'
    
    for alpha in alphas:
        subdir_name = 'alpha'+str(alpha)
        subdir_path = os.path.join(path, subdir_name)
        if(os.path.exists(subdir_path)==False):
            os.makedirs(subdir_path)
        prior_path = os.path.join(subdir_path, label+'.prior')
        modify_prior('prior.prior', prior_path, alpha, A_eff_min, A_eff_max, log_uniform)
        ini_path = os.path.join(subdir_path, label+'.ini')
        modify_ini(ini, ini_path, label, 'a'+str(alpha).replace(".","p")+'_'+label, prior_path, injection_path, local_gen=local_gen)
        os.chdir(subdir_path)
        os.system('bilby_pipe '+ini_path + ' --submit')
        os.chdir(cwd)
    
        
parser = argparse.ArgumentParser("Make and submit LIV config files")
parser.add_argument("--label", type=str, default='inj')
parser.add_argument("--a_eff_min", type=float, default=1e-24)
parser.add_argument("--a_eff_max", type=float, default=1e-12)
parser.add_argument('--local_gen', action=argparse.BooleanOptionalAction, default=False)
parser.add_argument("--log_uniform", action=argparse.BooleanOptionalAction, default=True)
if __name__ == "__main__":
    
    args = parser.parse_args()
    make_directory_structure(args.label, args.a_eff_min, args.a_eff_max, local_gen =args.local_gen, log_uniform=args.log_uniform)


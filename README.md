# LIV automation

Migrating LIV analysis to bilby and making it compatible with Asimov

## Parametrizations
1. Parametrization in log10_lambda_eff, used in O3 and implemented in lal_simulation
```math
\delta \Phi_{\alpha}(f)=sign(A_\alpha) \begin{cases}\frac{\pi D_L}{\alpha-1}\lambda_{A,eff}^{\alpha-2}(\frac f c)^{\alpha-1} & \alpha  \neq 1\\\frac{\pi D_L}{\lambda_{A,eff}}\ln(\frac{\pi G \mathcal{M}f}{c^3})& \alpha = 1\end{cases} 
```


2. Parametrization in A_eff, implemented in bilby as phase correction to the waveform

```math
\delta \Phi_{\alpha}(f)= -\frac{\pi D_L h^{\alpha-2}}{c}A_{\alpha,eff}(f )^{\alpha-1}
```


## Using the package

To use the package for LIV testing in bilby:
1. Choose the appropriate function in bilbyLIV/waveform.py as frequency_domain_source_model
2. Choose bilbyLIV.conversion.generate_all_bbh_parameters (or bns) as generation function to generate A_alpha in the posterior samples
3. In the prior, include a prior in A_eff (in peV^(2-alpha)). Additionaly include a delta prior in alpha (must be 0, 0.5, 1., 1.5, 2.5, 3.0, 3.5, 4.). Recommended prior is `A_eff = SymmetricLogUniform(name='A_eff', minimum=1e-24, maximum=1e-12, unit='$peV^{2-\\alpha}$', latex_label='$A_{eff}$')`

## Rerunning o3b events

To rerun o3b events in bilby_pipe with settings used in PE analysis:
1. Clone the project.
2. Make python environment with bilby_pipe, by running for example:
```
conda create -n <env_name> python=3.9
conda activate <env_name>
conda install -c conda-forge bilby_pipe bilby gwpy python-lalsimulation
```
3. Add bilbyLIV to your python path, by for example copying bilbyLIV folder to `~/.conda/envs/<env_name>/lib/python3.9/site-packages`.
4. Go to events and copy `generate_inis.py` to a subfolder of the event you want to run on. The names are consistent with ones in https://ldas-jobs.ligo.caltech.edu/~pe.o3/LVC/o3b-catalog/
5. While **in** the folder of the event you want to reanalyze, run `python generate_inis.py`. The script edits config files used in Asimov o3b analysis; creates appropriate config and prior files for all the values of alpha; and submits them to the cluster. It also accepts additional arguments: `--a_eff_min <float>`: prior minimum on A_eff, `--a_eff_max <float>`: prior maximum on A_eff (in peV^(2-alpha)), `--webdir_root <str>`: absolute path to html summary pages (at `/webdir_root/<event_name>/<alpha>`), `--local_gen`: local data generation - required for running on clusters other than CIT, `--no-log_uniform`: changes prior on A_eff from Symmetric Log Uniform to Uniform. Requires changing lower prior limit.
Default execution outside CIT should look like `python generate_inis.py --local_gen --webdir_root /home/albert.einstein/public_html/example_directory`
6. The results of each run will be created in `events/<event_name>/<alpha>/Prod*/final_result`.
7. Reweighting the posteriors can be done with `bilbyLIV.conversion.reweight_posterior`.
8. Sometimes the cluster has problems and the posteriors are not properly merged together. In this case, you have to manually merge the posteriors.
9. Default o3b analysis runs 4 parallel jobs. For making runs less resource intensive, you may consider editing `n-parallel=4` in `events/<event_name>/Prod*.prior` to a lower number, like 1.

## Link to results from O3

Just so I don't forget where it is
@CIT:/home/tgr.o3/o3b-tgr/results/liv/gwtc3_data 

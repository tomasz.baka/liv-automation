import os
import glob
import argparse


def modify_ini(file_name, output_ini, outdir, label, prior_name, webdir, local_gen=False):
    with open(file_name, 'r') as file:
        # read a list of lines into data
        data = file.readlines()
    data.append('\n')
    data.append('conversion-function=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters\n')
    data.append('generation-function=bilbyLIV.conversion.generate_all_bbh_parameters\n')
    #set outdir
    idx=[n for n, x in enumerate(data) if 'outdir=' in x]
    data[idx[0]]='outdir='+outdir+'\n'
    #set label
    idx=[n for n, x in enumerate(data) if 'label=' in x]
    data[idx[0]]='label='+label+'\n'
    
    #local generation
    idx=[n for n, x in enumerate(data) if 'local-generation=' in x]
    try:
        data[idx[0]]='local-generation={}\n'.format(str(local_gen))
    except:
        data.append('local-generation={}\n'.format(str(local_gen)))

    #set freq model
    idx=[n for n, x in enumerate(data) if 'frequency-domain-source-model=' in x]
    data[idx[0]]='frequency-domain-source-model=bilbyLIV.waveform.liv_binary_black_hole\n'

    #accounting
    idx=[n for n,x in enumerate(data) if 'accounting=' in x]
    if not idx:
        data.append('accounting=ligo.prod.o3.cbc.pe.lalinference\n')

    #file transfer
    idx=[n for n,x in enumerate(data) if 'transfer-files=' in x]
    if not idx:
        data.append('transfer-files=False\n')

    #set prior location
    idx=[n for n, x in enumerate(data) if 'prior-file=' in x]
    data[idx[0]]='prior-file='+prior_name+'\n'
    
    idx=[n for n, x in enumerate(data) if 'convert-to-flat-in-component-mass=' in x]
    try:
        data[idx[0]]='\n'
    except:
        pass
    
    #set calibration data
    idx=[n for n, x in enumerate(data) if 'spline-calibration-envelope-dict=' in x]
    text=data[idx[0]]
    interferometers = ['H1']*('H1:' in text)+ ['L1']*('L1:' in text)+ ['V1']*('V1:' in text)
    return_text='spline-calibration-envelope-dict={ '
    for ifo in interferometers:
        return_text+=ifo+':'+os.path.abspath(glob.glob('./*{}_cal.txt'.format(ifo))[0])+', '
    return_text+='}\n'
    data[idx[0]]=return_text

    #set psd location
    idx=[n for n, x in enumerate(data) if 'psd-dict' in x]
    text=data[idx[0]]
    interferometers = ['H1']*('H1:' in text)+ ['L1']*('L1:' in text)+ ['V1']*('V1:' in text)
    return_text='psd-dict={ '
    for ifo in interferometers:
        return_text+=ifo+':'+os.path.abspath(glob.glob('./*{}_psd.dat'.format(ifo))[0])+', '
    return_text+='}\n'
    data[idx[0]]=return_text
    
    #create summary
    idx=[n for n, x in enumerate(data) if 'create-summary=' in x]
    if not idx:
        data.append('create-summary=True\n')
    else:
        data[idx[0]]='create-summary=True\n'

    #ensure higher modes
    idx=[n for n, x in enumerate(data) if 'waveform-approximant=I' in x]
    data[idx[-1]]='waveform-approximant=IMRPhenomXPHM\n'

    #webdir
    idx=[n for n, x in enumerate(data) if 'webdir=' in x]
    if not idx:
        data.append('webdir='+ webdir + '\n')
    else:
        data[idx[0]]='webdir='+ webdir + '\n'

    
    
    idx=[n for n, x in enumerate(data) if 'create-plots=' in x]
    if not idx:
        data.append('plot-trace=True\n')
    else:
        data[idx[0]]='plot-trace=True\n'
    
    
    idx=[n for n, x in enumerate(data) if 'singularity-image=' in x]
    try:
        data[idx[0]]='\n'
    except:
        pass
    idx=[n for n, x in enumerate(data) if 'plot-marginal=' in x]
    if not idx:
        data.append('plot-marginal=True\n')
    else:
        data[idx[0]]='plot-marginal=True\n'
    

    idx=[n for n, x in enumerate(data) if 'plot-corner=' in x]
    if not idx:
        data.append('plot-corner=True\n')
    else:
        data[idx[0]]='plot-corner=True\n'

    #get rid of time marginalization
    idx=[n for n, x in enumerate(data) if 'time-marginalization=' in x]
    data[idx[0]]='time-marginalization=False\n'

    #get rid of distance marginalization
    idx=[n for n, x in enumerate(data) if 'distance-marginalization=' in x]
    data[idx[0]]='distance-marginalization=False\n'

    idx=[n for n, x in enumerate(data) if 'distance-marginalization-lookup-table=' in x]
    try:
        data[idx[0]]='distance-marginalization-lookup-table=None\n'
    except:
        pass

    data.append('request-disk=2\n')

    with open(output_ini, 'w') as f:
        for line in data:
            f.write(line)
            
def modify_prior(prior_name, modified_name, alpha, A_eff_min, A_eff_max, log_uniform=False):
    with open(prior_name, 'r') as file:
        # read a list of lines into data
        data = file.readlines()
        
    data.append('alpha = {:.1f}\n'.format(alpha))
    if log_uniform:
        data.append('A_eff = SymmetricLogUniform(name=\'A_eff\', minimum={}, maximum={}, unit=\'$peV^{{2-\\\\alpha}}$\', latex_label=\'$A_{{eff}}$\')'.format(A_eff_min, A_eff_max))
    else:
        data.append('A_eff = Uniform(name=\'A_eff\', minimum={}, maximum={}, unit=\'$peV^{{2-\\\\alpha}}$\', latex_label=\'$A_{{eff}}$\')'.format(A_eff_min, A_eff_max))
    with open(modified_name, 'w') as f:
        for line in data:
            f.write(line)
            
def check_if_bilby_ini(file_name):
    char_line='################################################################################'
    with open(file_name, 'r') as file:
        for line in file:
            if char_line in line:
                return True
    return False

def get_ini_prior():
    path = './'
    
    config_file = glob.glob(path+'Prod*.ini', recursive=False)[0]
    production = config_file.split('_config')[0].split('/')[-1]
    prior_file = glob.glob(path+'*.prior', recursive=False)[0]
    return config_file, prior_file, production
        

def make_directory_structure(A_eff_min=1e-24, A_eff_max=1e-14, webdir=None, local_gen =False, log_uniform=False):
    cwd=os.getcwd()
    path = cwd
    event_name=path.split('/')[-1]
    alphas=[0.0, 0.5, 1.0, 1.5, 2.5, 3., 3.5, 4.0]
    #alphas=[0.0]
    if (webdir is not None) and (webdir[-1]!='/'):
        webdir+='/'
    ini, prior, label=get_ini_prior()
    
    for alpha in alphas:
        subdir_name = 'alpha'+str(alpha)
        subdir_path = os.path.join(path, subdir_name)
        if(os.path.exists(subdir_path)==False):
            os.makedirs(subdir_path)
        prior_path = os.path.join(subdir_path, label+'.prior')
        modify_prior(prior, prior_path, alpha, A_eff_min, A_eff_max, log_uniform)
        ini_path = os.path.join(subdir_path, label+'.ini')
        if webdir is None:
            modify_ini(ini, ini_path, label, 'a'+str(alpha).replace(".","p")+'_'+event_name, prior_path, 'None', local_gen=local_gen)
        else:
            modify_ini(ini, ini_path, label, 'a'+str(alpha).replace(".","p")+'_'+event_name, prior_path, webdir+event_name+'/'+subdir_name+'/'+label,local_gen=local_gen)
        os.chdir(subdir_path)
        os.system('bilby_pipe '+ini_path + ' --submit')
        os.chdir(cwd)
    
        
parser = argparse.ArgumentParser("Make and submit LIV config files")
parser.add_argument("--a_eff_min", type=float, default=1e-24)
parser.add_argument("--a_eff_max", type=float, default=1e-12)
parser.add_argument("--webdir_root", type=str, default=None)
parser.add_argument('--local_gen', action=argparse.BooleanOptionalAction, default=False)
parser.add_argument("--log_uniform", action=argparse.BooleanOptionalAction, default=True)
if __name__ == "__main__":
    
    args = parser.parse_args()
    make_directory_structure(args.a_eff_min, args.a_eff_max, webdir=args.webdir_root, local_gen =args.local_gen, log_uniform=args.log_uniform)
        
